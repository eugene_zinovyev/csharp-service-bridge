﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceBridge.Facade.Contracts;
using ServiceBridge.Facade.Business;

namespace ServiceBridge.Facade.MonolithicApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly ICompositeValueService valueService;

        public ValuesController(ICompositeValueService valueService)
        {
            this.valueService = valueService;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<CompositeValueDto> Get()
        {
            return valueService.Get();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public CompositeValueDto Get(int id)
        {
            return valueService.Get(id);
        }
    }
}
