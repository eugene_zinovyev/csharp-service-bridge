﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceBridge.ServiceOne.Business;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.ServiceOne.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IValueService valueService;

        public ValuesController(IValueService valueService)
        {
            this.valueService = valueService;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<ValueDto> Get()
        {
            return valueService.Get();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ValueDto Get(int id)
        {
            return valueService.Get(id);
        }
    }
}
