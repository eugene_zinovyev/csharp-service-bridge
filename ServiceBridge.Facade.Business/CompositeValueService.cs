using System.Linq;
using System.Collections.Generic;
using ServiceBridge.Facade.Contracts;
using ServiceBridge.ServiceOne.Bridge.Abstractions;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.Facade.Business
{
    public class CompositeValueService : ICompositeValueService
    {
        private static readonly IDictionary<int, string> valueDescriptions =
            new Dictionary<int, string> {
                { 1, "Description One" },
                { 3, "Description Three" }
            };

        private readonly IValueServiceBridge valueServiceBridge;

        public CompositeValueService(IValueServiceBridge valueServiceBridge)
        {
            this.valueServiceBridge = valueServiceBridge;
        }


        public IReadOnlyCollection<CompositeValueDto> Get()
        {
            var values = valueServiceBridge.Get();
            return values.Select(x => CompositeValueDtoMapper.Map(x, GetDescription(x))).ToArray();
        }

        public CompositeValueDto Get(int id)
        {
            var value = valueServiceBridge.Get(id);
            return CompositeValueDtoMapper.Map(value, GetDescription(value));
        }

        private string GetDescription(ValueDto value)
        {
            if (valueDescriptions.ContainsKey(value.Id))
            {
                return valueDescriptions[value.Id];
            }
            return null;
        }
    }
}