using ServiceBridge.Facade.Contracts;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.Facade.Business
{
    public static class CompositeValueDtoMapper
    {
        public static CompositeValueDto Map(ValueDto value, string description)
        {
            return new CompositeValueDto
            {
                Id = value.Id,
                Name = value.Name,
                Description = description,
            };
        }
    }
}