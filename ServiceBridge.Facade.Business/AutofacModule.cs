﻿using System;
using Autofac;

namespace ServiceBridge.Facade.Business
{
    public class AutofacModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<CompositeValueService>().As<ICompositeValueService>();
        }
    }
}
