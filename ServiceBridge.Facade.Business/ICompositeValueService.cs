using System.Collections.Generic;
using ServiceBridge.Facade.Contracts;

namespace ServiceBridge.Facade.Business
{
    public interface ICompositeValueService
    {
        IReadOnlyCollection<CompositeValueDto> Get();

        CompositeValueDto Get(int id);
    }
}