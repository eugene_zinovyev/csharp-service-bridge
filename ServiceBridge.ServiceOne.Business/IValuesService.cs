using System.Collections.Generic;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.ServiceOne.Business
{
    public interface IValueService
    {
        IReadOnlyCollection<ValueDto> Get();

        ValueDto Get(int id);
    }
}