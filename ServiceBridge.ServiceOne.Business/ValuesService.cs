using System.Linq;
using System.Collections.Generic;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.ServiceOne.Business
{
    public class ValueService : IValueService
    {
        private static readonly IReadOnlyCollection<Value> values =
            new List<Value> {
                { new Value { Id = 1, Name = "Value One" } },
                { new Value { Id = 2, Name = "Value Two" } },
                { new Value { Id = 3, Name = "Value Three" } }
            };

        public IReadOnlyCollection<ValueDto> Get()
        {
            return values.Select(ValueMapper.Map).ToArray();
        }

        public ValueDto Get(int id)
        {
            return ValueMapper.Map(values.First(x => x.Id == id));
        }
    }
}