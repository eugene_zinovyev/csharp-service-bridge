using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.ServiceOne.Business
{
    public static class ValueMapper
    {
        public static ValueDto Map(Value value)
        {
            return new ValueDto
            {
                Id = value.Id, 
                Name = value.Name,
            };
        }
    }
}