﻿using System;
using Autofac;

namespace ServiceBridge.ServiceOne.Business
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ValueService>().As<IValueService>();
        }
    }
}
