using System.Collections.Generic;
using ServiceBridge.ServiceOne.Contracts;

namespace ServiceBridge.ServiceOne.Bridge.Abstractions
{
    public interface IValueServiceBridge
    {
        IReadOnlyCollection<ValueDto> Get();

        ValueDto Get(int id);
    }
}