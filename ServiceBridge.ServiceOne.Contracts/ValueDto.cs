using System.Runtime.Serialization;

namespace ServiceBridge.ServiceOne.Contracts
{
    [DataContract]
    public class ValueDto
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}