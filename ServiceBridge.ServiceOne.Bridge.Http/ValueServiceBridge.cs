using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using ServiceBridge.ServiceOne.Bridge.Abstractions;
using ServiceBridge.ServiceOne.Contracts;
using Newtonsoft.Json;

namespace ServiceBridge.ServiceOne.Bridge.Http
{
    public class ValueServiceBridge : IValueServiceBridge, IDisposable
    {
        private const string ValueControllerUrl = "http://localhost:5001/api/values";
        
        private readonly HttpClient httpClient;

        public ValueServiceBridge()
        {
            httpClient = new HttpClient();
        }

        public IReadOnlyCollection<ValueDto> Get()
        {
            return MakeHttpCall<IReadOnlyCollection<ValueDto>>(ValueControllerUrl);
        }

        public ValueDto Get(int id)
        {
            return MakeHttpCall<ValueDto>($"{ValueControllerUrl}/{id}");
        }

        private T MakeHttpCall<T>(string url)
        {
            var response = httpClient.GetAsync(url).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<T>(responseContent);
        }
 
        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}