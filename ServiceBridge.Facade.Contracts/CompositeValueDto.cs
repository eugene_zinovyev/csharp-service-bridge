﻿using System;
using System.Runtime.Serialization;

namespace ServiceBridge.Facade.Contracts
{
    [DataContract]
    public class CompositeValueDto
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
}
