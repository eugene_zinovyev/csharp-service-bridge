using System;
using System.Collections.Generic;
using ServiceBridge.ServiceOne.Bridge.Abstractions;
using ServiceBridge.ServiceOne.Contracts;
using ServiceBridge.ServiceOne.Business;

namespace ServiceBridge.ServiceOne.Bridge.DI
{
    public class ValueServiceBridge : IValueServiceBridge
    {
        private readonly IValueService valueService;

        public ValueServiceBridge(IValueService valueService)
        {
            this.valueService = valueService;
        }

        public IReadOnlyCollection<ValueDto> Get()
        {
            return valueService.Get();
        }

        public ValueDto Get(int id)
        {
            return valueService.Get(id); 
        }
    }
}