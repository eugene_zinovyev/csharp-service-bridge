﻿using System;
using Autofac;
using ServiceBridge.ServiceOne.Bridge.Abstractions;

namespace ServiceBridge.ServiceOne.Bridge.DI
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ValueServiceBridge>().As<IValueServiceBridge>();
        }
    }
}
